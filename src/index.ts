import 'phaser';
import { GameScene } from './scenes/Game';

const gameConfig: Phaser.Types.Core.GameConfig = {
    width: 1920,
    height: 1080,
    scene: GameScene
};

new Phaser.Game(gameConfig);