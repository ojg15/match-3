import { Gem } from '../objects/Gem';
import { GameScene } from '../scenes/Game';

export enum MatchLevel {
    ONE,
    TWO,
    THREE
}

export class Match {
    constructor(public gems: Gem[], public level?: MatchLevel) {
        if (!this.level) {
            switch (gems.length) {
                case 5: this.level = MatchLevel.THREE; break;
                case 4: this.level = MatchLevel.TWO; break;
                default: this.level = MatchLevel.ONE;
            }
        }
    }
}

export class MatchManager {

    private static _instance: MatchManager;
    public static get instance(): MatchManager {
        if (!this._instance) {
            this._instance = new MatchManager();
        }
        return this._instance;
    }

    private _scene: Phaser.Scene;

    public initialise(scene: Phaser.Scene) {
        this._scene = scene;
    }

    private addMatch(current: Match[], newMatchGems: Gem[]) {
        for (let i=0; i < current.length; i++) {
            for (let j=0; j < newMatchGems.length; j++) {
                if (current[i].gems.indexOf( newMatchGems[j] ) > -1) {
                    current[i].level = MatchLevel.THREE;
                    for (let k=0; k < newMatchGems.length; k++) {
                        if (k !== j) { current[i].gems.push(newMatchGems[k]); }
                    }
                    return;
                }
            }
        }
        current.push( new Match(newMatchGems) );
    }

    public checkMatches(gems: Gem[][]): Match[] {
        let matches: Match[] = [];

        // Checking along the columns
        for (let i=0; i < gems.length; i++) {
            let streak: Gem[] = [];
            for (let j=0; j < gems[i].length && j < GameScene.rowsVisible; j++) {
                if (streak.length === 0 || streak[streak.length - 1]._colour === gems[i][j]._colour) {
                    streak.push(gems[i][j]);
                } else if (streak.length > 2) {
                    matches.push( new Match(streak.slice(0)) );
                    streak = [ gems[i][j] ];
                } else {
                    streak = [ gems[i][j] ];
                }
            }
            if (streak.length > 2) { matches.push( new Match(streak.slice(0)) ); }
            streak = [];
        }

        // Checking along the rows
        for (let i=0; i < GameScene.rowsVisible; i++) {
            let streak: Gem[] = [];
            for (let j=0; j < gems.length; j++) {
                if (streak.length === 0 || streak[streak.length - 1]._colour === gems[j][i]._colour) {
                    streak.push(gems[j][i]);
                } else if (streak.length > 2) {
                    this.addMatch(matches, streak.slice(0));
                    streak = [ gems[j][i] ];
                } else {
                    streak = [ gems[j][i] ];
                }
            }
            if (streak.length > 2) { this.addMatch(matches, streak.slice(0)); }
            streak = [];
        }

        return matches;
    }

    public async replaceMatchedGems(gems: Gem[][], matches: Match[], sfxLevel:number, onGemInput: Function): Promise<void> {
        let updates: Set<Gem> = new Set<Gem>();
        for (let i=0; i < matches.length; i++) {

            let sfxSuffix: string = (sfxLevel === 0 || matches[i].level > MatchLevel.ONE) ? "" : `_${Math.min(sfxLevel, 2)}`;
            this._scene.sound.play(`match_${matches[i].level + 3}${sfxSuffix}`);

            for (let j=0; j < matches[i].gems.length; j++) {
                let col: number = matches[i].gems[j]._x;
                let row: number = matches[i].gems[j]._y;
                if (gems[col][row]) {
                    gems[col][row].matched(this._scene);
                    gems[col].splice(row, 1);
                    
                    let g: Gem = new Gem(this._scene, col, GameScene.rows, (p, g) => onGemInput(p, g));
                    g._sprite.displayWidth = GameScene.gemDiametre;
                    g._sprite.displayHeight = GameScene.gemDiametre;
                    gems[col].push(g);

                    // TODO this can bug out if game over is called in the middle of it
                    // ends up replacing all the gems and continuing on..
                    for (let k = row; k < GameScene.rows; k++) {
                        if (gems[col][k]) {
                            gems[col][k]._y -= 1;
                            updates.add(gems[col][k]);
                        }
                    }
                }
            }
        }

        let promises: Promise<void>[] = []
        for (const gem of updates.values()) {
            promises.push( gem.tweenToNewPosition(this._scene, 400) );
        };

        await Promise.all(promises);
    }

    public async swapGems(gem1: Gem, gem2: Gem): Promise<void> {
        let temp: any = {
            x: gem2._x,
            y: gem2._y
        };
        gem2._x = gem1._x; gem2._y = gem1._y;
		gem1._x = temp.x; gem1._y = temp.y;

		gem1.tweenToNewPosition(this._scene, 300);
		await gem2.tweenToNewPosition(this._scene, 300);
    }
}