import { GameScene } from '../scenes/Game';

export enum GemEnum {
    GREEN,
    ORANGE,
    YELLOW,
    BLUE,
    RED,
    PURPLE,
    COUNT
}

export class Gem {

    public static getRandomColour(): number {
        return Math.floor( Math.random() * GemEnum.COUNT );
    }

    public _x: number;
    public _y: number;
    public _colour: GemEnum;
    public _sprite: Phaser.GameObjects.Image;

    constructor(scene: Phaser.Scene, i: number, j: number, onDown: Function, colour?: GemEnum) {
        let gemX: number = GameScene.leftEdge + (i + 0.75) * GameScene.gemDiametre * (1 + GameScene.gemGap);
        let gemY: number = (GameScene.rowsVisible - j - 0.5) * GameScene.gemDiametre * 0.98;

        this._x = i;
        this._y = j;

        this._colour = colour === undefined ? Gem.getRandomColour() : colour;

        this._sprite = scene.add.image(gemX, gemY, 'gems', this._colour);
        this._sprite.setOrigin(0.5, 0.5);

        this._sprite.setInteractive();
        this._sprite.on('down', onDown);
        this._sprite.on('pointerdown', (pointer) => { this._sprite.emit('down', pointer, this); });
    }

    public async tweenToNewPosition(scene: Phaser.Scene, duration: number): Promise<void> {
        let newX: number = GameScene.leftEdge + (this._x + 0.75) * GameScene.gemDiametre * (1 + GameScene.gemGap);
		let newY: number = (GameScene.rowsVisible - this._y - 0.5) * GameScene.gemDiametre * 0.98;

        return new Promise<void>( (resolve) => {
            scene.tweens.add({
                targets: this._sprite,
                x: newX,
                y: newY,
                duration: duration,
                ease: 'Cubic.easeIn',
                onComplete: resolve
            });
        });
    }

    public async matched(scene: Phaser.Scene): Promise<void> {
        await new Promise<void>( (resolve) => {
            this._sprite.depth = 1;
            scene.tweens.add({
                targets: this._sprite,
                scaleX: this._sprite.scaleX * 1.5,
                scaleY: this._sprite.scaleY * 1.5,
                alpha: 0,
                duration: 250,
                ease: 'Cubic.easeOut',
                onComplete: resolve
            });
        });
        this._sprite.destroy();
        this._colour = null;
    }

    public toString(): string {
        return `${this._colour} [${this._x}, ${this._y}]`;
    }
}