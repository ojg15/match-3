import { MatchManager, Match, MatchLevel } from '../managers/MatchManager';
import { Gem } from '../objects/Gem';

let PointValues = new Map<MatchLevel, number>();
PointValues.set(MatchLevel.ONE, 100);
PointValues.set(MatchLevel.TWO, 300);
PointValues.set(MatchLevel.THREE, 800);

export class GameScene extends Phaser.Scene {

	public static width = window.innerWidth > window.innerHeight ? window.innerHeight : window.innerWidth;
    public static rows = 12;
    public static rowsVisible = 8;
    public static cols = 8;
    public static gemGap = 0.04;
    public static score = 0;

    public static get gemDiametre() {
        let gaps = (GameScene.cols - 1) * GameScene.gemGap + 0.5;
        return  Math.floor( GameScene.width / (GameScene.cols + gaps) );
    }
    public static leftEdge = (window.innerWidth - GameScene.width) * 0.5;

    private _timeLeft: number = 60000;

    private _gameRunning: boolean;
	private _gems: Gem[][];
	private _selectedGem;
	private _inputDownPos;
	private _matchingInProgress: boolean;

    private bg: Phaser.GameObjects.Image;

    constructor(config: string | Phaser.Types.Core.GameConfig) {
        super(config);

		this._gameRunning = true;
		this._matchingInProgress = false;
        this._gems = [];
    }

    public preload(): void {
        // this.load.image('bg', 'assets/bg.png');
        this.load.spritesheet('gems', 'assets/gems.png', { frameWidth: 128, frameHeight: 128 });
		this.load.audio('swap_gems', 'assets/sfx/swap_gems.ogg');
		this.load.audio('gems_fall', 'assets/sfx/gems_fall.ogg');
        this.load.audio('match_3', 'assets/sfx/match_3.ogg');
        this.load.audio('match_3_1', 'assets/sfx/match_3_1.ogg');
        this.load.audio('match_3_2', 'assets/sfx/match_3_2.ogg');
        this.load.audio('match_4', 'assets/sfx/match_4.ogg');
		this.load.audio('match_5', 'assets/sfx/match_5.ogg');
    }

    public create(): void {

        // this.bg = this.add.image(window.innerWidth * 0.5, window.innerHeight * 0.5, 'bg');
        // this.bg.setOrigin(0.5);
        // this.bg.setScale(window.innerWidth / this.bg.width);

        for (let i = 0; i < GameScene.cols; i++) {
            this._gems.push([]);
            for (let j = 0; j < GameScene.rows; j++) {
                let gem: Gem = new Gem(this, i, j, (p, g) => this.onGemInputDown(p, g));
                gem._sprite.displayWidth = GameScene.gemDiametre;
				gem._sprite.displayHeight = GameScene.gemDiametre;

                this._gems[i].push(gem);
            }
		}

		MatchManager.instance.initialise(this);
    }

    public update(time: number, delta: number): void {

		if (!this._gameRunning) { return; }
		
		this._timeLeft -= delta;
		if (this._timeLeft <= 0) {
			this._gameRunning = false;
			this._gems.forEach( (gemColumn: Gem[]) => {
				gemColumn.forEach( (gem: Gem) => { gem.matched(this); });
			});
		}
	}

	private async onGemInputDown(pointer: Phaser.Input.Pointer, gem: Gem) {
		if (this._matchingInProgress) { return; }

		if (this._selectedGem) {
			this.sound.play('swap_gems');
			await this.swapGems(gem._x, gem._y);
			this.input.off('pointermove');
			this.input.off('pointerup');
			this._selectedGem = undefined;
		} else {
			this._selectedGem = gem;
			this._inputDownPos = new Phaser.Geom.Point(pointer.position.x, pointer.position.y);
			this.input.on('pointermove', (p) => this.onInputMove(p));
			this.input.on('pointerup', (p) => this.onInputUp(p));
		}
	}

	private async onInputMove(pointer: Phaser.Input.Pointer) {
		if (this._matchingInProgress) { return; }

		let d = Phaser.Math.Distance.Between(pointer.position.x, pointer.position.y, this._inputDownPos.x, this._inputDownPos.y);
		if (d > 30) {
			let angle: number = Phaser.Math.Angle.Between(
				pointer.position.x, pointer.position.y, this._inputDownPos.x, this._inputDownPos.y
			);
			let deg = angle * 180 / Math.PI + 180;
			let toSwapX: number = this._selectedGem._x;
			let toSwapY: number = this._selectedGem._y;

			if (deg > 135 && deg < 225) { toSwapX -= 1; }
			else if (deg > 45 && deg < 135) { toSwapY -= 1; }
			else if (deg > 315 || deg < 45) { toSwapX += 1; }
			else { toSwapY += 1; }

			await this.swapGems(toSwapX, toSwapY);

			this.input.off('pointermove');
			this.input.off('pointerup');
			this._selectedGem = undefined;
		}
	}

	private async swapGems(x2: number, y2: number): Promise<void> {
		this._matchingInProgress = true;
		let x1: number = this._selectedGem._x;
		let y1: number = this._selectedGem._y;
		if (this._gems[x2] && this._gems[x2][y2]) {
			let temp: Gem = this._gems[x2][y2];
			this._gems[x2][y2] = this._gems[x1][y1];
			this._gems[x1][y1] = temp;

			await MatchManager.instance.swapGems(this._gems[x1][y1], this._gems[x2][y2]);

			let sfxLevel: number = 0;
			let matches: Match[] = MatchManager.instance.checkMatches(this._gems);
			while (matches.length > 0) {
				for (let m of matches) {
					let value = PointValues.get(m.level);
					GameScene.score += value;
				}
				await MatchManager.instance.replaceMatchedGems(this._gems, matches, sfxLevel, (p, g) => this.onGemInputDown(p, g));
				matches = MatchManager.instance.checkMatches(this._gems);
				sfxLevel++;
			}
			console.log(`GAME SCORE UPDATE: ${GameScene.score}`);

			this._matchingInProgress = false;
		}
	}

	private onInputUp(pointer: Phaser.Input.Pointer) {
		this.input.off('pointermove');
		this.input.off('pointerup');
	}
}